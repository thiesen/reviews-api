# frozen_string_literal: true

class CreateReviewThemes < ActiveRecord::Migration[6.0]
  def change
    create_table :review_themes do |t|
      t.belongs_to :review, null: false, foreign_key: true
      t.belongs_to :theme, null: false, foreign_key: true
      t.integer :sentiment, null: false, inclusion: -1..1

      t.timestamps
    end
  end
end
