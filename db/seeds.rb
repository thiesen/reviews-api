# frozen_string_literal: true

[Category, Review, Theme].each(&:destroy_all)

def seed_data_for(resource)
  seeds_data = File.read("db/seeds_data/#{resource}.json")

  JSON.parse(seeds_data)
end

seed_data_for('categories').each do |category_data|
  Category.create(category_data)
end

seed_data_for('themes').each do |theme_data|
  Theme.create(theme_data)
end

seed_data_for('reviews').each do |review_data|
  ReviewCreator.call(review_data)
end
