# frozen_string_literal: true

DeliveryBoy.configure do |config|
  config.client_id = 'api'

  config.brokers = ['kafka']
end
