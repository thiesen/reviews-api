# frozen_string_literal: true

class ReviewsController < ApplicationController
  rescue_from ActionController::ParameterMissing do |exception|
    render json: exception, status: :unprocessable_entity
  end

  def create
    service_response = ReviewCreator.call(review_params)

    if service_response.success?
      render json: service_response.record, status: :created
    else
      render json: service_response.record.errors, status: :unprocessable_entity
    end
  end

  private

  def review_params
    params.require(:review).permit(
      :id,
      :comment,
      themes: %i[theme_id sentiment]
    )
  end
end
