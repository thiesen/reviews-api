# frozen_string_literal: true

class ReviewCreated
  TOPIC = 'reviews'
  EVENT_NAME = 'review_created'

  def initialize(review)
    @review = review
  end

  def publish
    DeliveryBoy.deliver_async(
      payload.to_json,
      topic: TOPIC,
      key: review.id
    )
  end

  private

  attr_reader :review

  def payload
    {
      id: review.id,
      comment: review.comment,
      themes: themes_payload,
      created_at: review.created_at
    }
  end

  def themes_payload
    review.themes.map do |review_theme|
      {
        sentiment: review_theme.sentiment,
        theme: review_theme.name,
        category: review_theme.category_name
      }
    end
  end
end
