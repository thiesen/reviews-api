# frozen_string_literal: true

class ReviewCreator
  Response = Struct.new(:success?, :record, keyword_init: true)
  def self.call(params)
    new(params).call
  end

  def initialize(params)
    @review = Review.new(params)
  end

  def call
    with_exception_handling do
      review.save!

      publish_message

      Response.new(
        success?: true,
        record: review
      )
    end
  end

  private

  attr_reader :review

  def publish_message
    ReviewCreated.new(review).publish
  end

  def with_exception_handling
    yield
  rescue ActiveRecord::RecordInvalid
    Response.new(
      success?: false,
      record: review
    )
  end
end
