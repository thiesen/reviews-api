# frozen_string_literal: true

class Review < ApplicationRecord
  validates :comment, presence: true
  validates :themes, presence: true

  has_many :themes, dependent: :destroy, class_name: 'ReviewTheme'

  accepts_nested_attributes_for :themes

  # workaround to accept themes on requests without the need of a custom parser
  alias_attribute :themes=, :themes_attributes=
end
