# frozen_string_literal: true

class Theme < ApplicationRecord
  belongs_to :category
  has_many :review_themes, dependent: :destroy

  validates :name, presence: true
end
