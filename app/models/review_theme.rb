# frozen_string_literal: true

class ReviewTheme < ApplicationRecord
  belongs_to :review
  belongs_to :theme

  validates :sentiment, presence: true, inclusion: { within: (-1..1) }

  delegate :name, :category, to: :theme
  delegate :name, to: :category, prefix: true
end
