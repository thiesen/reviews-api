# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReviewsController, type: :controller do
  shared_examples_for 'unprocessable request' do
    it 'returns http status 422' do
      post_data

      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe 'POST #create' do
    subject(:post_data) do
      post :create, params: { review: params }
    end

    context 'with invalid params' do
      let(:params) { {} }

      it_behaves_like 'unprocessable request'
    end

    context 'with valid params' do
      context 'when theme params are missing' do
        let(:params) do
          {
            comment: 'Excellent keep it up',
            themes: []
          }
        end

        it_behaves_like 'unprocessable request'
      end

      context 'with theme data' do
        context 'when theme exists' do
          let!(:theme) { create(:theme) }

          let(:params) do
            {
              comment: 'Excellent keep it up',
              themes: [
                {
                  theme_id: theme.id,
                  sentiment: 1
                }
              ]
            }
          end

          it 'returns http status 201' do
            post_data

            expect(response).to have_http_status(:created)
          end

          it 'creates a new review' do
            expect { post_data }.to change { Review.count }.by(1)
          end

          it 'creates a new review theme' do
            expect { post_data }.to change { ReviewTheme.count }.by(1)
          end
        end

        context 'when theme does not exist' do
          let(:params) do
            {
              comment: 'Excellent keep it up',
              themes: [
                {
                  theme_id: 0,
                  sentiment: 1
                }
              ]
            }
          end

          it_behaves_like 'unprocessable request'
        end
      end
    end
  end
end
