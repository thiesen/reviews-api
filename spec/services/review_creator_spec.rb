# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReviewCreator do
  describe '.call' do
    subject(:review_creator) { described_class }

    context 'with valid params' do
      let(:base_params) do
        {
          comment: 'exelent'
        }
      end

      context 'when a single review theme is given' do
        let!(:theme) { create(:theme) }
        let(:theme_params) do
          [
            {
              theme_id: theme.id,
              sentiment: 0
            }
          ]
        end

        let(:params) do
          base_params.merge(
            themes: theme_params
          )
        end

        it 'returns a success response' do
          expect(review_creator.call(params)).to be_success
        end

        it 'creates a review record' do
          expect { review_creator.call(params) }
            .to change(Review, :count).by(1)
        end

        it 'creates a review theme' do
          expect { review_creator.call(params) }
            .to change(ReviewTheme, :count).by(1)
        end

        it 'publishes a message to kafka' do
          review_creator.call(params)

          messages = DeliveryBoy.testing.messages_for('reviews')

          expect(messages.count).to eq 1
        end
      end

      context 'when multiple review themes are given' do
        let!(:theme) { create(:theme) }
        let!(:theme2) { create(:theme) }
        let!(:theme3) { create(:theme) }

        let(:themes_params) do
          [
            {
              theme_id: theme.id,
              sentiment: 0
            },
            {
              theme_id: theme2.id,
              sentiment: -1
            },
            {
              theme_id: theme3.id,
              sentiment: 1
            }
          ]
        end

        let(:params) do
          base_params.merge(
            themes: themes_params
          )
        end
        it 'returns a success response' do
          expect(review_creator.call(params)).to be_success
        end

        it 'creates a review record' do
          expect { review_creator.call(params) }
            .to change(Review, :count).by(1)
        end

        it 'creates multiple review themes' do
          expect { review_creator.call(params) }
            .to change(ReviewTheme, :count).by(3)
        end
      end
    end

    context 'with invalid params' do
      let(:params) { {} }

      it 'returns a failed response' do
        expect(review_creator.call(params)).not_to be_success
      end

      it 'keeps the active model errors on the returned record object' do
        response = review_creator.call(params)
        record = response.record

        expect(record.errors).to be_a ActiveModel::Errors
      end

      it 'returns the errors' do
        response = review_creator.call(params)
        record = response.record

        expect(record.errors).to be_present
      end

      it 'does not create a review record' do
        expect { review_creator.call(params) }
          .not_to change(Review, :count)
      end

      it 'does not create a review theme record' do
        expect { review_creator.call(params) }
          .not_to change(Review, :count)
      end
    end
  end
end
