# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    sequence(:name) { |number| "category #{number}" }
  end
end
