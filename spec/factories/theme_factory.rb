# frozen_string_literal: true

FactoryBot.define do
  factory :theme do
    sequence(:name) { |number| "theme #{number}" }

    association :category
  end
end
