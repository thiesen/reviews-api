# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReviewCreated do
  describe '#publish' do
    let(:category) { create(:category) }
    let(:theme) { create(:theme, category: category) }
    let(:sentiment) { 1 }

    let(:review) do
      Review.create(
        comment: 'foo',
        themes: [
          {
            theme: theme,
            sentiment: sentiment
          }
        ]
      )
    end

    subject(:event) { described_class.new(review) }

    let(:expected_payload) do
      {
        comment: review.comment,
        themes: [
          {
            theme: theme.name,
            sentiment: sentiment,
            category: category.name
          }
        ],
        created_at: review.created_at,
        id: review.id
      }.with_indifferent_access
    end

    it 'publishes review as a json message to kafka' do
      event.publish

      messages = DeliveryBoy.testing.messages_for('reviews')

      expect(messages.count).to eq 1
    end

    it 'publishes message with the expected payload' do
      event.publish

      messages = DeliveryBoy.testing.messages_for('reviews')
      event = JSON.parse(messages.first.value).with_indifferent_access

      # workaround as I'm having trouble matching date time format.
      # maybe will try harder later :)
      expect(event[:created_at].to_date).to eq review.created_at.to_date

      expect(event.except(:created_at))
        .to match(expected_payload.except(:created_at))
    end
  end
end
