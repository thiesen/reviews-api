# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReviewTheme, type: :model do
  describe 'delegation' do
    it { is_expected.to delegate_method(:name).to(:theme) }
    it { is_expected.to delegate_method(:category).to(:theme) }
    it { is_expected.to delegate_method(:name).to(:category).with_prefix(true) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:review) }
    it { is_expected.to belong_to(:theme) }
  end

  describe 'validations' do
    it { is_expected.to validate_inclusion_of(:sentiment).in_range(-1..1) }
  end
end
