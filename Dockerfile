FROM ruby:latest

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev postgresql-client-11

RUN mkdir /api
WORKDIR /api

COPY Gemfile /api/Gemfile
COPY Gemfile.lock /api/Gemfile.lock
RUN bundle install

COPY . /api
